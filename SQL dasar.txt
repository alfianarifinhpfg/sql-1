1. Membuat Database
MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.004 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.004 sec)

MariaDB [(none)]> use myshop
Database changed

2. Membuat Table di Dalam Database
a. table users
MariaDB [myshop]> create table users(
    -> id integer auto_increment,
    -> nama varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );
MariaDB [myshop]> describe users
    -> ;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| nama     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.010 sec)

b. table categories
MariaDB [myshop]> create table categories(
    -> id integer auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.021 sec)
MariaDB [myshop]> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.012 sec)

c. table items
MariaDB [myshop]> create table items(
    -> id integer auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price integer(50),
    -> stock integer(50),
    -> categories_id integer(50),
    -> primary key(id),
    -> foreign key(categories_id) references categories(id)
    -> );
Query OK, 0 rows affected (0.018 sec)

MariaDB [myshop]> describe items;
+---------------+--------------+------+-----+---------+----------------+
| Field         | Type         | Null | Key | Default | Extra          |
+---------------+--------------+------+-----+---------+----------------+
| id            | int(11)      | NO   | PRI | NULL    | auto_increment |
| name          | varchar(255) | YES  |     | NULL    |                |
| description   | varchar(255) | YES  |     | NULL    |                |
| price         | int(50)      | YES  |     | NULL    |                |
| stock         | int(50)      | YES  |     | NULL    |                |
| categories_id | int(50)      | YES  | MUL | NULL    |                |
+---------------+--------------+------+-----+---------+----------------+
6 rows in set (0.012 sec)

3. memasukkan Data pada table
a. Users
MariaDB [myshop]> insert into users (nama, email, password)
    -> values ("John Doe", "john@doe.com", "john123"),("Jane Doe","jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.003 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from users;
+----+----------+--------------+-----------+
| id | nama     | email        | password  |
+----+----------+--------------+-----------+
|  1 | John Doe | john@doe.com | john123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.001 sec)

b. Categories
MariaDB [myshop]> insert into categories (name)
    -> values ("gadget"), ("cloth"), ("men"), ("women"), ("branded");
Query OK, 5 rows affected (0.004 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.001 sec)

c. Items
MariaDB [myshop]> insert into items (name, description, price, stock, categories_id)
    -> value ("Samsung b50", "hape KEREN dari merek Sumsang", 4000000, 100, 1), ("Uniklooh", "baju KEREN dari Brand ternama tak dikenal", 500000, 50, 2), ("IMHO Watch", "Jam tangan anak yang JUJUR banget", 2000000, 10, 1);
Query OK, 3 rows affected (0.003 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-------------------------------------------+---------+-------+---------------+
| id | name        | description                               | price   | stock | categories_id |
+----+-------------+-------------------------------------------+---------+-------+---------------+
|  1 | Samsung b50 | hape KEREN dari merek Sumsang             | 4000000 |   100 |             1 |
|  2 | Uniklooh    | baju KEREN dari Brand ternama tak dikenal |  500000 |    50 |             2 |
|  3 | IMHO Watch  | Jam tangan anak yang JUJUR banget         | 2000000 |    10 |             1 |
+----+-------------+-------------------------------------------+---------+-------+---------------+
3 rows in set (0.000 sec)

4. Mengambil Data dari Database
a. Mengambil data users
MariaDB [myshop]> select id, nama, email from users;
+----+----------+--------------+
| id | nama     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

b. Mengambil data items
MariaDB [myshop]> select * from items where price>1000000;
+----+-------------+-----------------------------------+---------+-------+---------------+
| id | name        | description                       | price   | stock | categories_id |
+----+-------------+-----------------------------------+---------+-------+---------------+
|  1 | Samsung b50 | hape KEREN dari merek Sumsang     | 4000000 |   100 |             1 |
|  3 | IMHO Watch  | Jam tangan anak yang JUJUR banget | 2000000 |    10 |             1 |
+----+-------------+-----------------------------------+---------+-------+---------------+
2 rows in set (0.001 sec)

MariaDB [myshop]> select * from items where name like "%Watch";
+----+------------+-----------------------------------+---------+-------+---------------+
| id | name       | description                       | price   | stock | categories_id |
+----+------------+-----------------------------------+---------+-------+---------------+
|  3 | IMHO Watch | Jam tangan anak yang JUJUR banget | 2000000 |    10 |             1 |
+----+------------+-----------------------------------+---------+-------+---------------+
1 row in set (0.001 sec)

c.menampilkan data items join dengan categories
MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.categories_id, categories.name from items inner join categories on items.categories_id=categories.id;
+-------------+-------------------------------------------+---------+-------+---------------+--------+
| name        | description                               | price   | stock | categories_id | name   |
+-------------+-------------------------------------------+---------+-------+---------------+--------+
| Samsung b50 | hape KEREN dari merek Sumsang             | 4000000 |   100 |             1 | gadget |
| Uniklooh    | baju KEREN dari Brand ternama tak dikenal |  500000 |    50 |             2 | cloth  |
| IMHO Watch  | Jam tangan anak yang JUJUR banget         | 2000000 |    10 |             1 | gadget |
+-------------+-------------------------------------------+---------+-------+---------------+--------+
3 rows in set (0.001 sec)

5. Mengubah Data dari Database
MariaDB [myshop]> update items set name = "Samsung b50", price = 2500000
    -> where id=1;
Query OK, 1 row affected (0.003 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-------------------------------------------+---------+-------+---------------+
| id | name        | description                               | price   | stock | categories_id |
+----+-------------+-------------------------------------------+---------+-------+---------------+
|  1 | Samsung b50 | hape KEREN dari merek Sumsang             | 2500000 |   100 |             1 |
|  2 | Uniklooh    | baju KEREN dari Brand ternama tak dikenal |  500000 |    50 |             2 |
|  3 | IMHO Watch  | Jam tangan anak yang JUJUR banget         | 2000000 |    10 |             1 |
+----+-------------+-------------------------------------------+---------+-------+---------------+
3 rows in set (0.001 sec)

